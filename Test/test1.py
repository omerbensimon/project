from selenium import webdriver
from selenium.webdriver.common.keys import Keys


driver = webdriver.Chrome("C:\\Users\\aaa\\PycharmProjects\\Proj1\\drivers\\chromedriver.exe")
driver.get("https://the-internet.herokuapp.com/context_menu")

el = "Right-click in the box below to see one called \'the-internet\'" in driver.page_source
assert el, "found :Right-click in the box below to see one called \'the-internet\'"

er = "Alibaba" in driver.page_source
assert er, "not-found: Alibaba"

driver.quit()
